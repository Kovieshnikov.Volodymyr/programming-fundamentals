﻿
namespace Lab10_1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picture = new System.Windows.Forms.PictureBox();
            this.CosButton = new System.Windows.Forms.Button();
            this.SineButton = new System.Windows.Forms.Button();
            this.TangentButton = new System.Windows.Forms.Button();
            this.CotangentButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.picture)).BeginInit();
            this.SuspendLayout();
            // 
            // picture
            // 
            this.picture.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picture.Location = new System.Drawing.Point(0, 0);
            this.picture.Name = "picture";
            this.picture.Size = new System.Drawing.Size(800, 550);
            this.picture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picture.TabIndex = 0;
            this.picture.TabStop = false;
            // 
            // CosButton
            // 
            this.CosButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CosButton.Location = new System.Drawing.Point(17, 501);
            this.CosButton.Name = "CosButton";
            this.CosButton.Size = new System.Drawing.Size(98, 36);
            this.CosButton.TabIndex = 1;
            this.CosButton.Text = "Cosine";
            this.CosButton.UseVisualStyleBackColor = true;
            this.CosButton.Click += new System.EventHandler(this.CosButton_Click);
            // 
            // SineButton
            // 
            this.SineButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SineButton.Location = new System.Drawing.Point(132, 501);
            this.SineButton.Name = "SineButton";
            this.SineButton.Size = new System.Drawing.Size(98, 36);
            this.SineButton.TabIndex = 1;
            this.SineButton.Text = "Sine";
            this.SineButton.UseVisualStyleBackColor = true;
            this.SineButton.Click += new System.EventHandler(this.SineButton_Click);
            // 
            // TangentButton
            // 
            this.TangentButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TangentButton.Location = new System.Drawing.Point(551, 501);
            this.TangentButton.Name = "TangentButton";
            this.TangentButton.Size = new System.Drawing.Size(98, 36);
            this.TangentButton.TabIndex = 1;
            this.TangentButton.Text = "Tangent";
            this.TangentButton.UseVisualStyleBackColor = true;
            this.TangentButton.Click += new System.EventHandler(this.TangentButton_Click);
            // 
            // CotangentButton
            // 
            this.CotangentButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CotangentButton.Location = new System.Drawing.Point(669, 502);
            this.CotangentButton.Name = "CotangentButton";
            this.CotangentButton.Size = new System.Drawing.Size(119, 36);
            this.CotangentButton.TabIndex = 1;
            this.CotangentButton.Text = "Cotangent";
            this.CotangentButton.UseVisualStyleBackColor = true;
            this.CotangentButton.Click += new System.EventHandler(this.CotangentButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 550);
            this.Controls.Add(this.CotangentButton);
            this.Controls.Add(this.TangentButton);
            this.Controls.Add(this.SineButton);
            this.Controls.Add(this.CosButton);
            this.Controls.Add(this.picture);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.picture)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picture;
        private System.Windows.Forms.Button CosButton;
        private System.Windows.Forms.Button SineButton;
        private System.Windows.Forms.Button TangentButton;
        private System.Windows.Forms.Button CotangentButton;
    }
}

