﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace Lab10_1
{
    public partial class Form1 : Form
    {
        private Bitmap _bitmap;
        private Graphics _graphics;
        private Pen _pen;

        public Form1()
        {
            InitializeComponent();

            InitImage();
        }

        private void InitImage()
        {
            _bitmap = new Bitmap(picture.Size.Width, picture.Size.Height);
            _graphics = Graphics.FromImage(_bitmap);
            _graphics.SmoothingMode = SmoothingMode.AntiAlias;
            _pen = new Pen(Color.Black, 2);
        }

        private void DrawAxis()
        {
            _graphics.DrawLine(_pen, 0f, picture.Size.Height / 2f, picture.Size.Width, picture.Size.Height / 2f);

            _graphics.DrawLine(_pen, picture.Size.Width / 2f - 40, picture.Size.Height, picture.Size.Width / 2f - 40, 0f);

            UpdateImage();
        }

        private void DrawSine()
        {
            var points = new List<PointF>();
            var scale = picture.Size.Height / 4;

            for (var i = 0; i <= picture.Width; i++)
            {
                var sin = (float)Math.Sin(DegreesToRads(i));
                sin *= -1f;
                points.Add(new PointF(i, sin * scale + scale + 120));
            }

            _graphics.DrawCurve(new Pen(Color.Red, 5), points.ToArray());

            UpdateImage();
        }

        private void DrawCosine()
        {
            var points = new List<PointF>();
            var scale = picture.Size.Height / 2;

            for (var i = 0; i <= picture.Width; i++)
            {
                var cos = (float)Math.Cos(DegreesToRads(i));
                cos *= -1;
                points.Add(new PointF(i, cos * scale + scale));
            }

            _graphics.DrawCurve(new Pen(Color.Blue, 5), points.ToArray());

            UpdateImage();
        }

        private void DrawTangent()
        {
            var points = new List<PointF>();
            var scale = picture.Size.Height / 2;

            var oldTanValue = Math.Tan(0);

            for (var i = -100; i <= picture.Width + 100; i++)
            {
                var newTanValue = Math.Tan(DegreesToRads(i));

                if (Math.Abs(newTanValue - oldTanValue) > 1)
                {
                    if (points.Count > 1)
                        _graphics.DrawCurve(new Pen(Color.BlueViolet, 5), points.ToArray());

                    points = new List<PointF>();

                    continue;
                }

                newTanValue *= -1;
                points.Add(new PointF(i, (float)newTanValue * scale + scale));
            }

            UpdateImage();
        }

        private void DrawCotangent()
        {
            var points = new List<PointF>();
            var scale = picture.Size.Height / 2;

            var oldTanValue = Math.Tan(0);

            for (var i = -100; i <= picture.Width + 100; i++)
            {
                var newTanValue = Math.Tan(DegreesToRads(i));

                if (Math.Abs(newTanValue - oldTanValue) > 1)
                {
                    if (points.Count > 1)
                        _graphics.DrawCurve(new Pen(Color.Green, 5), points.ToArray());

                    points = new List<PointF>();

                    continue;
                }

                points.Add(new PointF(i + 100, (float)newTanValue * scale + scale));
            }

            UpdateImage();
        }

        private double DegreesToRads(int i) => Math.PI * i / 180;

        private void UpdateImage() => picture.Image = _bitmap;

        private void CosButton_Click(object sender, EventArgs e)
        {
            _graphics.Clear(Color.White);
            DrawAxis();
            DrawCosine();
        }

        private void SineButton_Click(object sender, EventArgs e)
        {
            _graphics.Clear(Color.White);
            DrawAxis();
            DrawSine();
        }

        private void TangentButton_Click(object sender, EventArgs e)
        {
            _graphics.Clear(Color.White);
            DrawAxis();
            DrawTangent();
        }

        private void CotangentButton_Click(object sender, EventArgs e)
        {
            _graphics.Clear(Color.White);
            DrawAxis();
            DrawCotangent();
        }
    }
}
