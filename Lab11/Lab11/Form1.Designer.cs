﻿
namespace Lab11
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Canvas = new System.Windows.Forms.PictureBox();
            this.Obstacle1 = new System.Windows.Forms.PictureBox();
            this.Obstacle2 = new System.Windows.Forms.PictureBox();
            this.Obstacle3 = new System.Windows.Forms.PictureBox();
            this.Obstacle4 = new System.Windows.Forms.PictureBox();
            this.Obstacle8 = new System.Windows.Forms.PictureBox();
            this.Obstacle6 = new System.Windows.Forms.PictureBox();
            this.Obstacle5 = new System.Windows.Forms.PictureBox();
            this.Obstacle7 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.Canvas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Obstacle1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Obstacle2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Obstacle3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Obstacle4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Obstacle8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Obstacle6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Obstacle5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Obstacle7)).BeginInit();
            this.SuspendLayout();
            // 
            // Canvas
            // 
            this.Canvas.Location = new System.Drawing.Point(12, 12);
            this.Canvas.Name = "Canvas";
            this.Canvas.Size = new System.Drawing.Size(776, 426);
            this.Canvas.TabIndex = 0;
            this.Canvas.TabStop = false;
            // 
            // Obstacle1
            // 
            this.Obstacle1.BackColor = System.Drawing.SystemColors.Highlight;
            this.Obstacle1.Location = new System.Drawing.Point(262, 52);
            this.Obstacle1.Name = "Obstacle1";
            this.Obstacle1.Size = new System.Drawing.Size(278, 15);
            this.Obstacle1.TabIndex = 1;
            this.Obstacle1.TabStop = false;
            // 
            // Obstacle2
            // 
            this.Obstacle2.BackColor = System.Drawing.SystemColors.Highlight;
            this.Obstacle2.Location = new System.Drawing.Point(64, 92);
            this.Obstacle2.Name = "Obstacle2";
            this.Obstacle2.Size = new System.Drawing.Size(11, 203);
            this.Obstacle2.TabIndex = 2;
            this.Obstacle2.TabStop = false;
            // 
            // Obstacle3
            // 
            this.Obstacle3.BackColor = System.Drawing.SystemColors.Highlight;
            this.Obstacle3.Location = new System.Drawing.Point(697, 92);
            this.Obstacle3.Name = "Obstacle3";
            this.Obstacle3.Size = new System.Drawing.Size(15, 203);
            this.Obstacle3.TabIndex = 3;
            this.Obstacle3.TabStop = false;
            // 
            // Obstacle4
            // 
            this.Obstacle4.BackColor = System.Drawing.SystemColors.Highlight;
            this.Obstacle4.Location = new System.Drawing.Point(191, 333);
            this.Obstacle4.Name = "Obstacle4";
            this.Obstacle4.Size = new System.Drawing.Size(394, 14);
            this.Obstacle4.TabIndex = 4;
            this.Obstacle4.TabStop = false;
            // 
            // Obstacle8
            // 
            this.Obstacle8.BackColor = System.Drawing.SystemColors.Highlight;
            this.Obstacle8.Location = new System.Drawing.Point(1, 12);
            this.Obstacle8.Name = "Obstacle8";
            this.Obstacle8.Size = new System.Drawing.Size(16, 426);
            this.Obstacle8.TabIndex = 5;
            this.Obstacle8.TabStop = false;
            // 
            // Obstacle6
            // 
            this.Obstacle6.BackColor = System.Drawing.SystemColors.Highlight;
            this.Obstacle6.Location = new System.Drawing.Point(782, 12);
            this.Obstacle6.Name = "Obstacle6";
            this.Obstacle6.Size = new System.Drawing.Size(22, 426);
            this.Obstacle6.TabIndex = 6;
            this.Obstacle6.TabStop = false;
            // 
            // Obstacle5
            // 
            this.Obstacle5.BackColor = System.Drawing.SystemColors.Highlight;
            this.Obstacle5.Location = new System.Drawing.Point(1, 4);
            this.Obstacle5.Name = "Obstacle5";
            this.Obstacle5.Size = new System.Drawing.Size(803, 20);
            this.Obstacle5.TabIndex = 7;
            this.Obstacle5.TabStop = false;
            // 
            // Obstacle7
            // 
            this.Obstacle7.BackColor = System.Drawing.SystemColors.Highlight;
            this.Obstacle7.Location = new System.Drawing.Point(1, 426);
            this.Obstacle7.Name = "Obstacle7";
            this.Obstacle7.Size = new System.Drawing.Size(803, 24);
            this.Obstacle7.TabIndex = 8;
            this.Obstacle7.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Obstacle7);
            this.Controls.Add(this.Obstacle5);
            this.Controls.Add(this.Obstacle6);
            this.Controls.Add(this.Obstacle8);
            this.Controls.Add(this.Obstacle4);
            this.Controls.Add(this.Obstacle3);
            this.Controls.Add(this.Obstacle2);
            this.Controls.Add(this.Obstacle1);
            this.Controls.Add(this.Canvas);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.Canvas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Obstacle1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Obstacle2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Obstacle3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Obstacle4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Obstacle8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Obstacle6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Obstacle5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Obstacle7)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox Canvas;
        private System.Windows.Forms.PictureBox Obstacle1;
        private System.Windows.Forms.PictureBox Obstacle2;
        private System.Windows.Forms.PictureBox Obstacle3;
        private System.Windows.Forms.PictureBox Obstacle4;
        private System.Windows.Forms.PictureBox Obstacle8;
        private System.Windows.Forms.PictureBox Obstacle6;
        private System.Windows.Forms.PictureBox Obstacle5;
        private System.Windows.Forms.PictureBox Obstacle7;
    }
}

