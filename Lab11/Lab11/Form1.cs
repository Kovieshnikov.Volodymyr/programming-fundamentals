﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Lab11
{
    public partial class Form1 : Form
    {
        private Graphics _graphics;
        private Bitmap _bitmap;

        private Timer _movementTimer;
        private Timer _collisionTimer;

        private List<Rectangle> _obstacles;

        private Image _playerImage;
        private Rectangle _collider;
        private Point _startLocation;

        private Random _random;

        private int horizontalMovement;
        private int verticalMovement;
        private bool _isCollisionOn = true;

        public Form1()
        {
            InitializeComponent();

            _obstacles = new List<Rectangle>();
            _random = new Random();

            ConfigureCollision();
            InitObstacles();
            ConfigureImage();
            ConfigureTimer();
        }

        private void ConfigureCollision()
        {
            _collisionTimer = new Timer();
            _collisionTimer.Interval = 1000;
            _collisionTimer.Tick += OnCollisionTick;
        }

        private void ConfigureImage()
        {
            _bitmap = new Bitmap(Canvas.Width, Canvas.Height);
            _graphics = Graphics.FromImage(_bitmap);
           
            _playerImage = Image.FromFile("plane.jpg");
            _startLocation = new Point(Canvas.Width / 2, Canvas.Height / 2);
            _collider = new Rectangle(_startLocation, _playerImage.Size);

            Canvas.Image = _bitmap;
        }

        private void ConfigureTimer()
        {
            _movementTimer = new Timer();
            _movementTimer.Interval = 10;
            _movementTimer.Tick += OnMovementTick;
            _movementTimer.Enabled = true;
        }

        private void OnCollisionTick(object sender, EventArgs e)
        {
            _isCollisionOn = !_isCollisionOn;
            _collisionTimer.Stop();
        }

        private void OnMovementTick(object sender, EventArgs e)
        {
            DrawImage();
            CalculateImpact();
        }

        private void DrawImage()
        {
            _graphics.Clear(Canvas.BackColor);
            _graphics.DrawImage(_playerImage, _collider);

            Canvas.Image = _bitmap;
        }

        private void CalculateImpact()
        {
            if (horizontalMovement == 0 && verticalMovement == 0)
                RandomizeMovementVectors();

            foreach (var obstacle in _obstacles)
            {
                if (_collider.IntersectsWith(obstacle) && _isCollisionOn)
                {
                    RandomizeMovementVectors();

                    _isCollisionOn = !_isCollisionOn;
                    _collisionTimer.Start();

                }

                //if (_isCollisionOn)
                //    continue;

                //_isCollisionOn = true;
                //RandomizeMovementVectors();
            }
            _collider.X += 3 * horizontalMovement;
            _collider.Y += 3 * verticalMovement;

            if (_collider.Location.X > 700 || _collider.Location.X < -20 
                || _collider.Location.Y > 500 || _collider.Location.Y < -20)
            {
                _collider.Location = _startLocation;
                RandomizeMovementVectors();
            }
        }

        private void RandomizeMovementVectors()
        {
            horizontalMovement = _random.Next(-1, 2);
            verticalMovement = _random.Next(-1, 2);
        }

        public void InitObstacles()
        {
            _obstacles.Add(new Rectangle(Obstacle1.Location, Obstacle1.Size));
            _obstacles.Add(new Rectangle(Obstacle2.Location, Obstacle2.Size));
            _obstacles.Add(new Rectangle(Obstacle3.Location, Obstacle3.Size));
            _obstacles.Add(new Rectangle(Obstacle4.Location, Obstacle4.Size));
            _obstacles.Add(new Rectangle(Obstacle5.Location, Obstacle5.Size));
            _obstacles.Add(new Rectangle(Obstacle6.Location, Obstacle6.Size));
            _obstacles.Add(new Rectangle(Obstacle7.Location, Obstacle7.Size));
            _obstacles.Add(new Rectangle(Obstacle8.Location, Obstacle8.Size));
        }
    }
}
