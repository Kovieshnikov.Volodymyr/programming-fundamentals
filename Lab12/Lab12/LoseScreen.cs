﻿using System;
using System.Windows.Forms;

namespace Lab12
{
    public partial class LoseScreen : Form
    {
        private readonly GameScreen _gameScreen;
        private readonly StartMenu _startMenu;
        public LoseScreen(GameScreen gameScreen, StartMenu startMenu)
        {
            InitializeComponent();

            _gameScreen = gameScreen;
            _startMenu = startMenu;
        }

        private void TryAgain(object sender, EventArgs e)
        {
            _gameScreen.RestartGame();
            Close();
        }

        private void ReturnToStartScreen(object sender, EventArgs e)
        {
            _startMenu.Show();
            _gameScreen.Close();
            Close();
        }
    }
}
