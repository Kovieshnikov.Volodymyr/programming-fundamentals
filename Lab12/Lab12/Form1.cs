﻿using Lab12.Properties;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Lab12
{
    public partial class GameScreen : Form
    {
        private bool _goLeft, _goRight;

        private int _dropSpeed = 8;
        private int _score;
        private int _missed;

        private Random _randX = new Random();
        private Random _randY = new Random();

        StartMenu _startMenu;
        PictureBox _splash = new PictureBox();

        public GameScreen(StartMenu startMenu)
        {
            InitializeComponent();
            ConfigureSplashImage();
            RestartGame();

            _startMenu = startMenu;
        }

        private void ConfigureSplashImage()
        {
            _splash.Image = Resources.splash;
            _splash.Height = 60;
            _splash.Width = 60;
            _splash.BackColor = Color.Transparent;
        }

        private void GameUpdateLoop(object sender, EventArgs e)
        {
            txtCaught.Text = "Caught: " + _score;
            txtMissed.Text = "Missed: " + _missed;

            MovePlayer();
            CatchEggs();

            if (_score > 10)
                _dropSpeed += 2;

            if (_missed > 5)
            {
                GameTimer.Stop();
                new LoseScreen(this, _startMenu).Show();
            }
        }

        private void MovePlayer()
        {
            if (_goLeft && player.Left > 0)
            {
                player.Left -= 12;
                player.Image = Resources.chicken_normal2;
            }

            if (_goRight && player.Left + player.Width < ClientSize.Width)
            {
                player.Left += 12;
                player.Image = Resources.chicken_normal;
            }
        }

        private void CatchEggs()
        {
            foreach (Control egg in Controls)
            {
                if (!(egg is PictureBox) || (string)egg.Tag != "eggs") 
                    continue;

                egg.Top += _dropSpeed;

                if (egg.Top + egg.Height > ClientSize.Height)
                {
                    _splash.Location = egg.Location;

                    Controls.Add(_splash);

                    egg.Top = _randY.Next(80, 300) * -1;
                    egg.Left = _randX.Next(5, ClientSize.Width - egg.Width);

                    _missed++;
                    player.Image = Resources.chicken_hurt;
                }

                if (player.Bounds.IntersectsWith(egg.Bounds))
                {
                    egg.Top = _randY.Next(80, 300) * -1;
                    egg.Left = _randX.Next(5, ClientSize.Width - egg.Width);
                    _score++;
                }
            }
        }

        private void KeyIsDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Left:
                    _goLeft = true;
                    break;

                case Keys.Right:
                    _goRight = true;
                    break;
            }
        }

        private void KeyIsUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Left:
                    _goLeft = false;
                    break;

                case Keys.Right:
                    _goRight = false;
                    break;
            }
        }

        public void RestartGame()
        {
            foreach (Control egg in Controls)
            {
                if (egg is PictureBox && (string)egg.Tag == "eggs")
                {
                    egg.Top = _randY.Next(80, 301) * -1;
                    egg.Left = _randX.Next(5, ClientSize.Width - egg.Width);
                }
            }

            player.Left = ClientSize.Width / 2;
            player.Image = Resources.chicken_normal;

            _score = 0;
            _missed = 0;
            _dropSpeed = 8;

            _goLeft = false;
            _goRight = false;

            GameTimer.Start();
        }
    }
}
