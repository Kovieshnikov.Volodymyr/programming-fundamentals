﻿using System;
using System.Windows.Forms;

namespace Lab12
{
    public partial class StartMenu : Form
    {
        public StartMenu()
        {
            InitializeComponent();
        }

        private void LoadGame(object sender, EventArgs e)
        {
            var gameScreen = new GameScreen(this);
            gameScreen.Show();
            Hide();
        }

        private void ShowHelp(object sender, EventArgs e)
        {
            var helpScreen = new HelpScreen();
            helpScreen.Show();
        }

        private void QuitGame(object sender, EventArgs e) =>
            Application.Exit();
    }
}
