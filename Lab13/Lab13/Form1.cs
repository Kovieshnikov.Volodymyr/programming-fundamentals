﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace Lab13
{
    public partial class Form1 : Form
    {
        private int _speed;
        private int _score;
        private int _health;
        private bool _gameOver;
        private Random _random;
        private Rectangle _collider;

        public Form1()
        {
            InitializeComponent();

            _random = new Random();
            RestartGame();
        }

        private void MainTimerEvent(object sender, EventArgs e)
        {
            TxtScore.Text = "Score: " + _score;
            TxtHealth.Text = "Health: " + _health;

            if (_gameOver == true)
            {
                GameTimer.Stop();
                TxtScore.Text = "Score: " + _score + " Гру завершено, натисніть Enter для перезапуску";
            }


            foreach (Control control in Controls)
            {
                if ((string)control.Tag == "Baloon" || (string)control.Tag == "Bomb")
                {
                    control.Top -= _speed;

                    if ((string)control.Tag == "Bomb" && control.Top < -100)
                        SetRandomPosition(control);

                    if ((string)control.Tag == "Baloon" && 
                        control.Top < -100 || 
                        control.Bounds.IntersectsWith(_collider))
                    {
                        DecreaseHealth();

                        SetRandomPosition(control);
                    }
                }
            }

            if (_score > 7)
                _speed = 8;

            if (_score > 15)
                _speed = 12;
        }

        private void DecreaseHealth()
        {
            _health--;
            TxtHealth.Text = "Health: " + _health;

            if (_health <= 0)
                _gameOver = true;
        }

        private void PopBaloon(object sender, EventArgs e)
        {
            if (_gameOver == true)
                return;

            var baloon = sender as PictureBox ?? throw new NoNullAllowedException("Baloon object is null");

            SetRandomPosition(baloon);

            _score++;
        }

        private void Boom(object sender, EventArgs e)
        {
            if (_gameOver == true)
                return;

            Bomb.Image = Properties.Resources.boom;
            _gameOver = true;
        }

        private void KeyIsUp(object sender, KeyEventArgs e)
        {
            if (_gameOver == true && e.KeyCode == Keys.Enter)
                RestartGame();
        }

        private void RestartGame()
        {
            _speed = 5;
            _score = 0;
            _health = 3;
            _gameOver = false;
            _collider = InitCollider();

            Bomb.Image = Properties.Resources.bomb; //якщо у попередній спробі бомбу взірвали

            foreach (Control control in Controls)
                if (control is PictureBox)
                    SetRandomPosition(control);

            GameTimer.Start();
        }

        private void SetRandomPosition(Control control)
        {
            control.Top = _random.Next(750, 1000);
            control.Left = _random.Next(5, 500);
        }

        private Rectangle InitCollider()
        {
            Rectangle result = default;
            foreach (Control control in Controls)
            {
                if(control.Name == "Obstacle")
                {
                    result = new Rectangle();
                    result.Location = control.Location;
                    result.Size = control.Size;
                }
            }

            return result;
        }
    }
}
