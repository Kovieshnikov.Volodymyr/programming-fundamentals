﻿
namespace Lab13
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.Bomb = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.TxtScore = new System.Windows.Forms.Label();
            this.GameTimer = new System.Windows.Forms.Timer(this.components);
            this.TxtHealth = new System.Windows.Forms.Label();
            this.Obstacle = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bomb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Obstacle)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Lab13.Properties.Resources.redHeart;
            this.pictureBox1.Location = new System.Drawing.Point(40, 541);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(111, 116);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Tag = "Baloon";
            this.pictureBox1.Click += new System.EventHandler(this.PopBaloon);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Lab13.Properties.Resources.redStar;
            this.pictureBox2.Location = new System.Drawing.Point(362, 541);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(111, 116);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Tag = "Baloon";
            this.pictureBox2.Click += new System.EventHandler(this.PopBaloon);
            // 
            // Bomb
            // 
            this.Bomb.Image = global::Lab13.Properties.Resources.bomb;
            this.Bomb.Location = new System.Drawing.Point(505, 559);
            this.Bomb.Name = "Bomb";
            this.Bomb.Size = new System.Drawing.Size(100, 98);
            this.Bomb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.Bomb.TabIndex = 0;
            this.Bomb.TabStop = false;
            this.Bomb.Tag = "Bomb";
            this.Bomb.Click += new System.EventHandler(this.Boom);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Lab13.Properties.Resources.pinkBalloon;
            this.pictureBox4.Location = new System.Drawing.Point(505, 397);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(100, 143);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox4.TabIndex = 0;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Tag = "Baloon";
            this.pictureBox4.Click += new System.EventHandler(this.PopBaloon);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::Lab13.Properties.Resources.yellowBalloon;
            this.pictureBox5.Location = new System.Drawing.Point(207, 541);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(111, 116);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox5.TabIndex = 0;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Tag = "Baloon";
            this.pictureBox5.Click += new System.EventHandler(this.PopBaloon);
            // 
            // TxtScore
            // 
            this.TxtScore.AutoSize = true;
            this.TxtScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TxtScore.Location = new System.Drawing.Point(12, 24);
            this.TxtScore.Name = "TxtScore";
            this.TxtScore.Size = new System.Drawing.Size(88, 24);
            this.TxtScore.TabIndex = 1;
            this.TxtScore.Text = "Score: 0";
            // 
            // GameTimer
            // 
            this.GameTimer.Interval = 20;
            this.GameTimer.Tick += new System.EventHandler(this.MainTimerEvent);
            // 
            // TxtHealth
            // 
            this.TxtHealth.AutoSize = true;
            this.TxtHealth.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TxtHealth.Location = new System.Drawing.Point(529, 24);
            this.TxtHealth.Name = "TxtHealth";
            this.TxtHealth.Size = new System.Drawing.Size(93, 24);
            this.TxtHealth.TabIndex = 1;
            this.TxtHealth.Text = "Health: 0";
            // 
            // Obstacle
            // 
            this.Obstacle.BackColor = System.Drawing.Color.PaleVioletRed;
            this.Obstacle.Dock = System.Windows.Forms.DockStyle.Top;
            this.Obstacle.Location = new System.Drawing.Point(0, 0);
            this.Obstacle.Name = "Obstacle";
            this.Obstacle.Size = new System.Drawing.Size(634, 89);
            this.Obstacle.TabIndex = 2;
            this.Obstacle.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PaleTurquoise;
            this.ClientSize = new System.Drawing.Size(634, 661);
            this.Controls.Add(this.TxtHealth);
            this.Controls.Add(this.TxtScore);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.Bomb);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.Obstacle);
            this.Name = "Form1";
            this.Text = "Шарик - Ба-Бах";
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyIsUp);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bomb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Obstacle)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox Bomb;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label TxtScore;
        private System.Windows.Forms.Timer GameTimer;
        private System.Windows.Forms.Label TxtHealth;
        private System.Windows.Forms.PictureBox Obstacle;
    }
}

